﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEditor;

namespace Visyde
{
    [CustomEditor(typeof(ShopItemEntity))]
    public class ShopItemEntityEditor : Editor
    {
        SerializedProperty id;
        SerializedProperty itemType;
        SerializedProperty cost;
        SerializedProperty character;
        SerializedProperty weapon;
        SerializedProperty mapName;
        SerializedProperty cosmetic;
        SerializedProperty itemPreview;

        void OnEnable(){
            id = serializedObject.FindProperty("id");
            itemType = serializedObject.FindProperty("itemType");
            cost = serializedObject.FindProperty("cost");
            character = serializedObject.FindProperty("character");
            weapon = serializedObject.FindProperty("weapon");
            mapName = serializedObject.FindProperty("mapName");
            cosmetic = serializedObject.FindProperty("cosmetic");
            itemPreview = serializedObject.FindProperty("itemPreview");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            // Generate and display the item ID:
            if (string.IsNullOrEmpty(id.stringValue))
            {
                id.stringValue = Guid.NewGuid().ToString();
            }

            GUI.backgroundColor = new Color(1f, 1f, 0.5f);
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Item ID: ", GUILayout.MaxWidth(52));
            EditorGUILayout.BeginHorizontal("box");
            EditorGUILayout.SelectableLabel(id.stringValue, GUILayout.MaxHeight(17));
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndHorizontal();
            GUI.backgroundColor = Color.white;

            GUILayout.Space(20);

            EditorGUILayout.BeginVertical("box");
            EditorGUILayout.PropertyField(cost);
            EditorGUILayout.PropertyField(itemType);

            // Display the appropriate fields based on the item type:
            switch (itemType.enumValueIndex)
            {
                case 0:
                    EditorGUILayout.PropertyField(character);
                    break;
                case 1:
                    EditorGUILayout.PropertyField(weapon);
                    break;
                case 2:
                    // Use this item's name as map name if mapName is empty:
                    if (string.IsNullOrEmpty(mapName.stringValue))
                    {
                        mapName.stringValue = target.name;
                    }
                    EditorGUILayout.PropertyField(mapName);
                    break;
                case 3:
                    EditorGUILayout.PropertyField(cosmetic);
                    break;
            }
            EditorGUILayout.EndVertical();

            // Item preview:
            bool isWeapon = itemType.enumValueIndex == 1;
            bool isCosmetic = itemType.enumValueIndex == 3;
            EditorGUILayout.BeginVertical("box");
            if (isWeapon && !weapon.objectReferenceValue)
            {
                EditorGUILayout.HelpBox("Please specify the weapon to display a preview.", MessageType.Info);
            }
            else if (isCosmetic && !cosmetic.objectReferenceValue){
                EditorGUILayout.HelpBox("Please specify the cosmetic to display a preview.", MessageType.Info);
            }
            else
            {
                if (isWeapon) GUI.enabled = false;
                itemPreview.objectReferenceValue = EditorGUILayout.ObjectField(isWeapon ? ((Weapon)weapon.objectReferenceValue).hudIcon 
                : isCosmetic ? ((CosmeticItemData)cosmetic.objectReferenceValue).icon 
                : itemPreview.objectReferenceValue, typeof(Sprite), false, GUILayout.Width(70), GUILayout.Height(70));
                GUI.enabled = true;
            }
            EditorGUILayout.EndVertical();

            serializedObject.ApplyModifiedProperties();
        }
    }
}
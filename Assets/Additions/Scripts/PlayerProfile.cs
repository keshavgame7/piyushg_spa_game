using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using SimpleJSON;

namespace Visyde
{
    /// <summary>
    /// Player Profile
    /// - Creates a "player profile" data that contains username, gold coins, and items owned
    /// - Contains
    /// - Data is saved by converting the class to JSON then saving to PlayerPrefs, process is reversed on loading
    /// </summary>
    public class PlayerProfile : MonoBehaviour
    {
        public static PlayerProfile instance;

        // Debugging:
        public bool simulateAllItemsOwned;

        public UserData data { get; protected set; }

        public string username => data.username;
        public int goldCoins => data.goldCoins;

        public UnityAction onProfileLoaded;
        public UnityAction onProfileSaved;
        
        bool isLoaded;

        public bool OwnsItem(string id){
            if (simulateAllItemsOwned || data.ContainsItem(id)) return true;
            return false;
        }
        public bool IsItemAffordable(ShopItemEntity item){
            return data.goldCoins >= item.cost;
        }

        void Awake()
        {
            if (instance)
            {
                Destroy(this);
                return;
            }
            instance = this;

        }

        // Start is called before the first frame update
        void Start()
        {
            // *Enable this for instant access:
            Load();
        }

        // Update is called once per frame
        void Update()
        {
            // For debugging:
            //if (Input.GetKeyDown(KeyCode.Space) && Input.GetKey(KeyCode.LeftControl))
            if (Input.GetKeyDown(KeyCode.T))
            {
                data.ResetInventory();
                Save();
                Load();

                print("Inventory data has been reset!");
            }
            if (Input.GetKeyDown(KeyCode.Space) && Input.GetKey(KeyCode.RightControl))
            {
                data.goldCoins = 100;
                Save();
                Load();

                print("Gold coins has been reset!");
            }
        }

        public bool TryAcquireItem(string id){

            // Try get the item first:
            ShopItemEntity item = GlobalShopItems.instance.GetItem(id);
            if (!item) return false;

            // If item isn't owned yet and is affordable, proceed:
            if (!data.ContainsItem(id) && SpendCoins(item.cost))
            {
                data.itemsOwned.Add(id);
                Save();
                Load();
                return true;
            }

            // Return false if unsuccessful
            return false;
        }

        public void AddCoins(int amount){
            data.goldCoins += amount;

            // Save player profile:
            if (isLoaded) Save();
        }
        /// <returns>Returns true if successful (amount is affordable).</returns>
        public bool SpendCoins(int amount){

            Load();

            if (data.goldCoins >= amount)
            {
                data.goldCoins -= amount;
                Save();
                return true;
            }

            return false;
        }

        public void Save()
        {
            PlayerPrefs.SetString("userData", data.GetSaveString());

            if (onProfileSaved != null) onProfileSaved();
        }
        public void Load()
        {
            if (PlayerPrefs.HasKey("userData"))
            {
                //data = JsonUtility.FromJson<UserData>(PlayerPrefs.GetString("userData"));
                data = new UserData();
                data.Load();

            }
            else{
                data = new UserData();
            }

            if (onProfileLoaded != null) onProfileLoaded();
            isLoaded = true;
        }
    }



    

    [System.Serializable]
    public class UserData
    {
        public string username;
        public int goldCoins;

        [SerializeField]
        public Dictionary<string, int> AchievementsProgress = new Dictionary<string, int>();

        [SerializeField]
        public Dictionary<string, int> AchievementsUnlocked = new Dictionary<string, int>();

        [SerializeField]
        public Dictionary<string, int> AchievementsShowed = new Dictionary<string, int>();

        public List<string> itemsOwned = new List<string>();

        public string GetSaveString(){
            JSONObject json = new JSONObject();
            
            json.Add("username",username);
            json.Add("coins",goldCoins);
            JSONObject AchProgress = new JSONObject();
            foreach (var item in AchievementsProgress)
            {
                AchProgress.Add(item.Key,item.Value);
            }
            json.Add("AchievementProgress",AchProgress);
            JSONObject AchUnlocked = new JSONObject();
            foreach (var item in AchievementsUnlocked)
            {
                AchUnlocked.Add(item.Key,item.Value);
            }
            JSONArray itmOwned = new JSONArray();
            foreach (var item in itemsOwned)
            {
                itmOwned.Add(item);
            }
            json.Add("itemsUnlocked",itmOwned);
            return json.ToString();
        }

        public void Load(){
            string dataStr = PlayerPrefs.GetString("userData");
            JSONObject obj = (JSONObject) JSON.Parse(dataStr);
            username = obj["username"];
            goldCoins = obj["coins"];
            AchievementsProgress = new Dictionary<string, int>();
            foreach (var item in obj["AchievementProgress"])
            {
                AchievementsProgress.Add(item.Key,item.Value);
            }
            AchievementsUnlocked = new Dictionary<string, int>();
            foreach (var item in obj["AchievementsUnlocked"])
            {
                AchievementsUnlocked.Add(item.Key,item.Value);
            }
            itemsOwned = new List<string>();
            foreach (var item in obj["itemsUnlocked"])
            {
                //Debug.Log(item.Value);
                itemsOwned.Add(item.Value);
            }
        }

        public bool ContainsItem(string id){
            
            if (itemsOwned.Contains(id)) return true;
            return false;
        }
        public void ResetInventory()
        {
            // Init values here based on game's library
            itemsOwned = new List<string>();
            AchievementsProgress = new Dictionary<string, int>();
            AchievementsUnlocked = new Dictionary<string, int>();
            AchievementsShowed = new Dictionary<string, int>();
        }

        public void AddAchievementProgress(string key, int amt){
            if(AchievementsProgress.ContainsKey(key))
                AchievementsProgress[key] += amt;
            else 
                AchievementsProgress.Add(key,amt);
            
            if(Application.platform != RuntimePlatform.Android && Application.platform != RuntimePlatform.IPhonePlayer){
                if(AchievementManager.manager != null)
                    AchievementManager.manager.StartCheckAchievements();
            }
            
        }

        public void SetAchievementProgress(string key, int amt){
            if(AchievementsProgress.ContainsKey(key))
                AchievementsProgress[key] = amt;
            else 
                AchievementsProgress.Add(key,amt);
        }

        public int GetAchievementProgress(string key){
            return AchievementsProgress.ContainsKey(key) ? AchievementsProgress[key] : 0;
        }

        public void UnlockAchivement(string id){
            if(AchievementsUnlocked.ContainsKey(id)){
                AchievementsUnlocked[id] = 1;
            }else{
                AchievementsUnlocked.Add(id,1);
            }
        }

        public void ShowAchivement(string id){
            if(AchievementsShowed.ContainsKey(id)){
                AchievementsShowed[id] = 1;
            }else{
                AchievementsShowed.Add(id,1);
            }
        }

        public bool IsAchivementUnlocked(string id){
            return AchievementsUnlocked.ContainsKey(id) ? AchievementsUnlocked[id] == 1 : false;
        }

        public bool IsAchivementShowed(string id){
            return AchievementsShowed.ContainsKey(id) ? AchievementsShowed[id] == 1 : false;
        }
    }
}
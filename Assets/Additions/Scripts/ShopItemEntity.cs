﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visyde
{
    /// <summary>
    /// This is for items which cannot be directly referenced as objects such as maps or without actual item data like weapons.
    /// </summary>
    [CreateAssetMenu(fileName = "New Shop Item", menuName = "Additions/Shop Item Entity")]
    public class ShopItemEntity : ScriptableObject
    {
        public string id;

        public ItemType itemType = ItemType.Character;
        public int cost;

        public CharacterData character;
        public Weapon weapon;
        public string mapName;
        public CosmeticItemData cosmetic;

        public Sprite itemPreview;

        
    }
    public enum ItemType
    {
        Character,
        Weapon,
        Map,
        Cosmetic
    }
}
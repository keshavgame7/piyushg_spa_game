﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace Visyde
{
    public class CoinController : MonoBehaviourPunCallbacks
    {
        public float fallSpeed = 1;
        public float groundOffset;
        public float spawnYOffset;
        public LayerMask worldColliders;
        public GameObject graphics;
        public AudioClip pickupSFX;

        public PlayerInstance owner { get; protected set; }

        //Turtorial Coin instatiation data
        public int ownerIdForTutorialCoin;
        public bool shouldFallValueForTutorialCoin;

        bool hasBeenPickedUp;
        bool shouldFall;
        Vector3? targetPos = null;
        float curGrav = 0;

        // Start is called before the first frame update
        void Start()
        {
            if(GameManager.instance.IsTutorial) 
            {
                SetInstantiationDataForTutorial();
            }
            else 
            {
                // Recognize owner (if spawned by the map then owner is set to null):
                int ownerId = (int)photonView.InstantiationData[0];
                owner = ownerId > -1 ? GameManager.instance.GetPlayerInstance((int)photonView.InstantiationData[0]) : null;
                // Should this coin be affected by gravity:
                shouldFall = (bool)photonView.InstantiationData[1];
            }
            
            // Spawn with added offset in the Y axis:
            if (shouldFall) transform.position += Vector3.up * spawnYOffset;
        }

        // Update is called once per frame
        void Update()
        {
            if (!shouldFall) return;

            if (!targetPos.HasValue || transform.position.y > targetPos.Value.y)
            {
                curGrav += Physics2D.gravity.y * fallSpeed * Time.deltaTime;
                transform.position -= Vector3.down * curGrav * Time.deltaTime;  // *time delta 
            }
            else{
                curGrav = 0;
            }
        }

        void FixedUpdate()
        {
            if (!shouldFall) return;

            RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, Vector2.down, 10, worldColliders);
            if (hits.Length > 0)
            {
                for (int i = 0; i < hits.Length; i++)
                {
                    if (hits[i].collider.CompareTag("Untagged") && hits[i].collider.gameObject != gameObject)
                    {
                        Vector3 pos = hits[i].point;
                        pos.y += groundOffset;
                        targetPos = pos;
                        return;
                    }
                }
            }
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player") && !hasBeenPickedUp)
            {
                PlayerController p = other.GetComponent<PlayerController>();
                if (p)
                {
                    hasBeenPickedUp = true;

                    // Sound and VFX:
                    //Instantiate(itemHandled.pickUpEffect, transform.position, Quaternion.identity);
                    graphics.SetActive(false);
                    p.PlaySFX(pickupSFX);

                    // Only the master client should handle the actual pick up event to prevent dupes:
                    if (PhotonNetwork.IsMasterClient && !GameManager.instance.IsTutorial)
                    {
                        photonView.RPC("PickedUp", RpcTarget.AllViaServer, p.playerInstance.playerID);
                    }
                    else 
                    {
                        PickedUp(p.playerInstance.playerID);
                    }
                }
            }
        }

        [PunRPC]
        public void PickedUp(int playerID){
            hasBeenPickedUp = true;

            // Add coin if we are the picker:
            if (GameManager.instance.GetPlayerInstance(playerID).isMine || GameManager.instance.IsTutorial){

                //Do not add coins for tutorial
                if(!GameManager.instance.IsTutorial)
                {
                    PlayerProfile.instance.AddCoins(1);
                    PlayerProfile.instance.data.AddAchievementProgress(AchievementType.CoinsCollected.ToString(),1);
                }
            } 

            //Add coins to the player profile
            GameManager.instance.AddScore(GameManager.instance.GetPlayerInstance(playerID),false,false,0,1 * GameManager.instance.pointPerCoin);

            // Destroy:
            Destroy(gameObject);
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position + new Vector3(-0.5f, -groundOffset), transform.position + new Vector3(0.5f, -groundOffset));
        }
        void SetInstantiationDataForTutorial()
        {
            // Recognize owner (if spawned by the map then owner is set to null):
            int ownerId = ownerIdForTutorialCoin;
            owner = ownerId > -1 ? GameManager.instance.GetPlayerInstance(ownerIdForTutorialCoin) : null;
            // Should this coin be affected by gravity:
            shouldFall = shouldFallValueForTutorialCoin;
        }
    }   
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visyde
{
    public class GlobalShopItems : MonoBehaviour
    {
        public static GlobalShopItems instance;

        public ShopItemEntity[] shopItems;

        /// <summary>
        /// Returns any item with matching ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns null if no item with the ID exists.</returns>
        public ShopItemEntity GetItem(string id){
            for (int i = 0; i < shopItems.Length; i++){
                if (shopItems[i].id == id) return shopItems[i];
            }

            return null;
        }

        public ShopItemEntity GetCosmetic(CosmeticItemData data){
            if (!data) return null;

            for (int i = 0; i < shopItems.Length; i++)
            {
                if (shopItems[i].cosmetic == data) return shopItems[i];
            }

            return null;
        }

        public ShopItemEntity GetCharacter(CharacterData data){

            if (!data) return null;

            for (int i = 0; i < shopItems.Length; i++)
            {
                if (shopItems[i].itemType == ItemType.Character && shopItems[i].character == data) return shopItems[i];
            }

            return null;
        }
        public ShopItemEntity GetMap(string mapName){
            
            if (string.IsNullOrEmpty(mapName)) return null;

            for (int i = 0; i < shopItems.Length; i++){
                if (shopItems[i].itemType == ItemType.Map && shopItems[i].mapName == mapName) return shopItems[i];
            }

            return null;
        }

        void Awake(){
            instance = this;
        }
    }
}
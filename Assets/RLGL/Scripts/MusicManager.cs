using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour
{

    private AudioSource source;

    public AudioClip MainMenuSFX;

    private static MusicManager manager;

    private float speed = 0.5f;

    private string LastsceneChange;

    public static MusicManager Get(){
        return manager;
    }

    public void SetVol(float v){
        source.volume = v;
    }


    private void Awake()
    {
        if(manager == null){
            manager = this;
            DontDestroyOnLoad(gameObject);
        }else{
            Destroy(gameObject);
        }

        source = GetComponent<AudioSource>();
        
    }

    // Start is called before the first frame update

    void Start()
    {
        
        if(PlayerPrefs.GetInt("Music",1) == 1){
            LastsceneChange  = SceneManager.GetActiveScene().name;
            if(SceneManager.GetActiveScene().name == "MainMenu"){
                StartCoroutine(TransitToMainMenu());
            }else if(SceneManager.GetActiveScene().name == "Game"){
                StartCoroutine(TransitToLevel());
            }
        }
        
        
    }

    // Update is called once per frame
    void Update()
    {
        if(LastsceneChange != SceneManager.GetActiveScene().name && PlayerPrefs.GetInt("Music",1) == 1){ 
            if(SceneManager.GetActiveScene().name == "MainMenu"){
                StartCoroutine(TransitToMainMenu());
            }else if(SceneManager.GetActiveScene().name == "Game"){
                StartCoroutine(TransitToLevel());
            }
            LastsceneChange = SceneManager.GetActiveScene().name;
        }
    }
    public void triggerMusic(){
        if(source.isPlaying)
            return;
        if(SceneManager.GetActiveScene().name == "MainMenu"){
            StartCoroutine(TransitToMainMenu());
        }else if(SceneManager.GetActiveScene().name == "Game"){
            StartCoroutine(TransitToLevel());
        }
        LastsceneChange = SceneManager.GetActiveScene().name;
        
    }

    IEnumerator TransitToMainMenu(){ //Slowly fade to main menu music
        float i = source.volume;
        while(i >= 0){
            i-= Time.deltaTime * speed; 
            source.volume = i;
            yield return null;
        }
        source.volume = 0;
        source.Stop();
        
        yield return null;

        source.clip = MainMenuSFX;
        source.Play();
        source.volume = 0f;
        

        yield return null;

        i = 0;
        while (i >= 1){
            i+= Time.deltaTime * speed;
            source.volume = i;
            yield return null;
        }
        source.volume = 0.2f;
    }

    IEnumerator TransitToLevel(){ //Slowly fade to Current level music
        float i = source.volume;
        while(i >= 0){
            i-= Time.deltaTime * speed;
            source.volume = i;
            yield return null;
        }
        source.volume = 0;
        source.Stop();
        yield return null;

        source.clip = Visyde.GameManager.instance.getActiveMap.LevelMusic;
        source.Play();
        source.volume = 0;

        yield return null;

        i = 0;
        while (i >= 1){
            i+= Time.deltaTime * speed;
            source.volume = i;
            yield return null;
        }
        source.volume = 0.2f;
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Visyde
{
    /// <summary>
    /// Character Selector Item
    /// - The component for item that is populated in the character selection screen.
    /// </summary>

    public class CharacterSelectorItem : MonoBehaviour
    {

        public CharacterData data;

        public TextMeshProUGUI nameText;
        public Image icon;
        public Fillbar hp;
        public Fillbar ms;
        public Fillbar jf;
        public Fillbar gr;
        public Fillbar dmg;
        public Fillbar ps;
        public Image startingWeaponIcon;
        public GameObject selectButton;
        public Button buyButton;
        public Text costText;
        public GameObject lockedOverlay;

        [HideInInspector] public CharacterSelector cs;
        [HideInInspector] public ShopItemEntity shopItem;

        void Start()
        {
            shopItem = GlobalShopItems.instance.GetCharacter(data);

            nameText.text = data.name;
            icon.sprite = data.icon;
            
            hp.value = data.maxHealth;
            ms.value = data.moveSpeed;
            jf.value = data.jumpForce;
            gr.value = data.grenades;
            dmg.value = data.startingWeapon.damage;
            ps.value = data.startingWeapon.projectileSpeed;

            startingWeaponIcon.sprite = data.startingWeapon.hudIcon;

            if (shopItem) costText.text = shopItem.cost.ToString();

            bool owned = shopItem ? PlayerProfile.instance.OwnsItem(shopItem.id) : true;
            selectButton.SetActive(owned);
            buyButton.gameObject.SetActive(!owned);
            //if (shopItem) buyButton.interactable = PlayerProfile.instance.IsItemAffordable(shopItem);
            lockedOverlay.SetActive(!owned);
        }

        public void Select()
        {
            if (!shopItem || PlayerProfile.instance.OwnsItem(shopItem.id)) cs.SelectCharacter(data);
        }
        public void Buy()
        {
            SampleMainMenu.instance.OpenPurchaseScreen(shopItem);
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation_pause : MonoBehaviour
{
    Animator anim;

    void Start()
    {
        StartCoroutine(PlayAnimation());
    }
    IEnumerator PlayAnimation()
    {
        while (true)
        {
            anim.SetBool("play", true);
            yield return new WaitForSeconds(1);
            anim.SetBool("play", false);
        }
    }

}

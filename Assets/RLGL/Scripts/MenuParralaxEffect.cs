using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuParralaxEffect : MonoBehaviour
{

    public GameObject[] parralaxObjs;
    [SerializeField]
    private float MouseSpeedX = 1f, MouseSpeedY = .2f;
    private Vector3[] orgPos;

    public Vector2 bounds;
    private Gyroscope gyro;
    public float sensorScale = 0.3f;
    public float Smoothening = 3f;
    float LastX;
    
    private void OnEnable()
    {
        gyro = Input.gyro;
        gyro.enabled  = true;
    }

    private void OnDisable()
    {
        if(gyro != null)
            gyro.enabled  = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        //Cursor.lockState = CursorLockMode.Confined;
        orgPos = new Vector3[parralaxObjs.Length];
        for (int i = 0; i < parralaxObjs.Length; i++)
        {
            orgPos[i] = parralaxObjs[i].transform.position;
        }
    }

    private void FixedUpdate()
    {
        float x,y;
        //Debug.LogError("Getting gyro : "+gyro.rotationRateUnbiased.x+" - "+gyro.rotationRateUnbiased.y);
        if(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer){
            bounds = new Vector2(2f,bounds.y);
            
            y = 0;//- ((float)System.Math.Round(gyro.rotationRate.y,1)) * sensorScale;
            for (int i = 1; i < parralaxObjs.Length + 1; i++)
            {
                x = Mathf.Clamp(((float)System.Math.Round(gyro.rotationRateUnbiased.y,1)) * sensorScale * i * ((i - 1) - (parralaxObjs.Length / 2)) ,-bounds.x, bounds.x) * (1f / Smoothening);
                
                //Vector3 newPos = (new Vector3(x,y,0f) * i * ((i - 1) - (parralaxObjs.Length / 2)));
                parralaxObjs[i - 1].transform.Translate(new Vector3(x,y,0f));
                
                Vector3 tempPos = parralaxObjs[i - 1].transform.position;
                if(Mathf.Abs(Mathf.Clamp(tempPos.x,orgPos[i - 1].x - bounds.x, orgPos[i - 1].x + bounds.y) - x) > 0.05f){
                    parralaxObjs[i - 1].transform.position = new Vector3(Mathf.Clamp(tempPos.x,orgPos[i - 1].x - bounds.x, orgPos[i - 1].x + bounds.y),tempPos.y,tempPos.z);
                    LastX = Mathf.Clamp(tempPos.x,orgPos[i - 1].x - bounds.x, orgPos[i - 1].x + bounds.y);
                }
                    
            }
        }else{
            x = (Input.mousePosition.x - (Screen.width / 2f)) * MouseSpeedX / Screen.width;
            y = (Input.mousePosition.y - (Screen.height / 2f)) * MouseSpeedY / Screen.height;
            for (int i = 1; i < parralaxObjs.Length + 1; i++)
            {
                Vector3 newPos = (new Vector3(x,y,0f) * i * ((i - 1) - (parralaxObjs.Length / 2)));
                parralaxObjs[i - 1].transform.position = orgPos[i - 1] + new Vector3(Mathf.Clamp(newPos.x,-bounds.x,bounds.x),Mathf.Clamp(newPos.y,-bounds.y,bounds.y),newPos.z);
            }
        }
        

        

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

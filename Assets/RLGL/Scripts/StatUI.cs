using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class StatUI : MonoBehaviour
{

    public GameObject TextPref;
    public string StatName;

    private float hoverTimer;
    private GameObject currentText;
    private bool IsOpen;
    private bool IsHovering;
    public Color TextColor;

    // Start is called before the first frame update
    void Start()
    {
        currentText = Instantiate(TextPref,transform.position + Vector3.up, Quaternion.identity);
        currentText.transform.SetParent(transform);
        currentText.transform.position = Vector3.zero;
        currentText.transform.localPosition = Vector3.up;
        currentText.transform.localScale = Vector3.one * 1.5f;
        currentText.GetComponentInChildren<TextMeshProUGUI>().text = StatName;
        currentText.GetComponent<Image>().color = TextColor;
        currentText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(IsHovering){
            hoverTimer += Time.deltaTime;
            if(hoverTimer >= 0.7f){
                //Debug.Log("sasa");
                currentText.SetActive(true);
            }
        }
        
    }

    public void OnPointerDown(){
        if(IsOpen){
            IsOpen = false;
            Invoke(nameof(DisableText),2f);
        }else{
            IsOpen = true;
            currentText.SetActive(true);
        }
    }

    public void OnPointerEnter(){
        //  Debug.Log("sasa");
        // hoverTimer += Time.deltaTime;
        // if(hoverTimer >= 0.7f){
        //     Debug.Log("sasa");
        //     currentText.SetActive(true);
        // }
        IsHovering = true;
    }
    public void OnPointerExit(){
        IsHovering = false;
        hoverTimer = 0;
        Invoke(nameof(DisableText),2f);
        IsOpen = false;
    }

    

    private void OnMouseDown()
    {
        if(IsOpen){
            IsOpen = false;
            Invoke(nameof(DisableText),2f);
        }else{
            IsOpen = true;
            currentText.SetActive(true);
        }
    }

    private void OnMouseOver()
    {
        Debug.Log("sasa");
        hoverTimer += Time.deltaTime;
        if(hoverTimer >= 0.7f){
            Debug.Log("sasa");
            currentText.SetActive(true);
        }
    }

    public void DisableText(){
        currentText.SetActive(false);
    }

    private void OnMouseExit()
    {
        Invoke(nameof(DisableText),2f);
    }
}

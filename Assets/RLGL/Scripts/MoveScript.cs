using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveScript : MonoBehaviour
{

    public float moveSpeed = 1.0f;
    public Vector3 moveVector;
    //public bool projectileOn();

    void Update()
    {
        {
            // Move the object upward in world space 1 unit/second.
            transform.Translate(moveVector * moveSpeed * Time.deltaTime);
        }
    }
}

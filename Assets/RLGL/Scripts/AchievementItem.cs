using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementItem : MonoBehaviour
{

    public Text Name;
    public Text description;

    public void show(){
        LeanTween.scale(gameObject, Vector3.one,0.4f);
        GetComponent<CanvasGroup>().alpha = 1f;
        StartCoroutine(Disappear(5f));
    }

    IEnumerator Disappear(float time){
        yield return new WaitForSeconds(time);
        CanvasGroup group = GetComponent<CanvasGroup>();
        float t = 1;
        group.alpha = t;
        while(t < 0){
            group.alpha = t;
            t -= Time.deltaTime;
            yield return null;
        }
        group.alpha = 0;
        Destroy(gameObject);
    }

    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

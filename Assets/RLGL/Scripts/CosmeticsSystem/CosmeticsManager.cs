﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visyde
{
    /// <summary>
    /// Cosmetics Manager
    /// - Manages all the player cosmetic side of things
    /// - Attached on the player prefab (along with the PlayerController component)
    /// </summary>

    public class CosmeticsManager : MonoBehaviour
    {
        // Internals:
        public int chosenHat;
        public int chosenHair;

        // Only this parameters matters
        public int CosmeticChosen = -1;
        PlayerController player;
        List<GameObject> spawnedItems = new List<GameObject>();

        void OnEnable(){
            player = GetComponent<PlayerController>();

            if (!player){
                Destroy(this);
                Debug.LogError("Cosmetics Manager should be attach to the same object the Player Controller is attached to.");
            }
        }

        public void Refresh(Cosmetics cosmetics){
            chosenHat = cosmetics.hat;
            chosenHair = cosmetics.hair;
            CosmeticChosen = cosmetics.cosmetic;
            Refresh();
        }
        public void Refresh()
        {
            // Remove existing cosmetic items if there's any:
            for (int i = 0; i < spawnedItems.Count; i++)
            {
                if (spawnedItems[i]) Destroy(spawnedItems[i]);
            }
            spawnedItems = new List<GameObject>();
            //Set the item to the specific location on the character. Now Use SamepleInventory to fetch the item. 
            if(CosmeticChosen >= 0){
                Transform cosmeticParent = null;
                if(SampleInventory.instance.items[CosmeticChosen].customPointIndex != -1){
                    cosmeticParent = player.character.CustomSettings[SampleInventory.instance.items[CosmeticChosen].customPointIndex];
                }else{
                    switch (SampleInventory.instance.items[CosmeticChosen].itemType)
                    {
                        case CosmeticType.Hat:
                            cosmeticParent = player.character.hatPoint;
                            break;
                        case CosmeticType.Hair:
                            cosmeticParent = player.character.hairPoint;
                            break;
                        case CosmeticType.Headsets:
                            cosmeticParent = player.character.HeadsetPoint;
                            break;
                        case CosmeticType.Masks:
                            cosmeticParent = player.character.MaskPoint;
                            break;
                        case CosmeticType.Ears:
                            cosmeticParent = player.character.EarPoint;
                            break;
                        case CosmeticType.Eye:
                            cosmeticParent = player.character.EyePoint;
                            break;
                        default:
                            cosmeticParent = player.character.hatPoint;
                            break;

                    }
                }
                
                GameObject item = Instantiate(SampleInventory.instance.items[CosmeticChosen].prefab, cosmeticParent);
                ResetItemPosition(item.transform);
                spawnedItems.Add(item);
            }
            // // If has hat:
            // if (chosenHat >= 0)
            // {
            //     GameObject item = Instantiate(ItemDatabase.instance.hats[chosenHat].prefab, player.character.hatPoint);
            //     ResetItemPosition(item.transform);
            //     spawnedItems.Add(item);
            // }

            // if(chosenHair>=0){
            //     GameObject item = Instantiate(ItemDatabase.instance.hairs[chosenHair].prefab, player.character.hairPoint);
            //     ResetItemPosition(item.transform);
            //     spawnedItems.Add(item);
        }
            // }

        void ResetItemPosition(Transform item){
            item.localEulerAngles = Vector3.zero;
            item.localPosition = Vector3.zero;
            item.localScale = Vector3.one;
        }
    }
}
﻿using UnityEngine;
using UnityEngine.UI;

namespace Visyde
{
    /// <summary>
    /// Inventory Slot
    /// - Sample main menu inventory slot.
    /// - Each slot has this component.
    /// </summary>

    public class InventorySlot : MonoBehaviour
    {
        public CosmeticItemData curItem;
        public Image iconImage;
        public GameObject check;

        public GameObject lockOverlay;

        [HideInInspector] public CharacterCustomizer cc;
        [HideInInspector] public ShopItemEntity shopItem;

        public void Refresh(){
            
            // Retrieve the shop item:
            shopItem = GlobalShopItems.instance.GetCosmetic(curItem);

            // Display icon:
            if (curItem){
                iconImage.sprite = curItem.icon;
                iconImage.enabled = true;
            }
            else{
                iconImage.enabled = false;
            }

            // Show/hide the `locked` overlay (just like the `CharacterSelectorItem.cs`):
            bool owned = shopItem ? PlayerProfile.instance.OwnsItem(shopItem.id) : true;
            lockOverlay.SetActive(!owned);

            // Show/hide checkmark:
            check.SetActive(DataCarrier.chosenHat >= 0 && ItemDatabase.instance.hats[DataCarrier.chosenHat] == curItem);
        }

        public void Select()
        {
            // Instead of directly equipping the cosmetic on selection, check wether it's owned:
            //if (curItem) cc.Equip(curItem);
            
            // If owned, equip:
            if (!shopItem || PlayerProfile.instance.OwnsItem(shopItem.id))
            {
                cc.Equip(curItem);
            }
            // If not, open the purchase screen:
            else{
                Buy();
            }
        }
        public void Buy()
        {
            SampleMainMenu.instance.OpenPurchaseScreen(shopItem);
        }
    }
}
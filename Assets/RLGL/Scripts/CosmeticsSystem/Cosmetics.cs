﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visyde
{
    /// <summary>
    /// Cosmetics
    /// - Used by the PlayerInstance class.
    /// - Downloaded and readable version of a player's cosmetic data.
    /// </summary>

    public class Cosmetics
    {
        public int cosmetic;
        public int hat;
        public int hair;
        public int backpack;        // sample only, no effect at the moment
                                    // add more items here...

        public Cosmetics(int cosmetic,int hat = -1,int hair = -1, int backpack = -1)
        {
            this.cosmetic = cosmetic;
            this.hat = hat;
            this.backpack = backpack;
            this.hair = hair;
        }
    }
}
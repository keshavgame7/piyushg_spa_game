using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Visyde;

public class LabBeam : MonoBehaviour
{

    public bool IsHit;
    public int damage;
    public float Timeout;
    public float BeamDuration;
    private float timer;
    private Animator animator;
    public static bool isBeamOn;
    public static int Damage;
    public float move_speed = 10f;
    public float map_size;

    private Vector3 dir = Vector3.right;
    private Vector2 bounds;


    




    // Start is called before the first frame update
    void Start()
    {
        animator =  GetComponent<Animator>();
        Damage = damage;
        bounds = GameManager.instance.maps[GameManager.instance.chosenMap].bounds;
    }

    // Update is called once per frame
    void Update()
    {
        
        timer += Time.deltaTime;

        if(timer >= Timeout && !isBeamOn){
            timer = 0;
            isBeamOn = true;
            animator.SetTrigger("Charge");
            Vector3 pos =  Vector3.zero;
            pos.y = transform.position.y;
            pos.z = transform.position.z;
            pos.x = Random.Range(-bounds.x,bounds.x);
            transform.position = pos;
            dir = Random.value > 0.5 ? Vector3.left : Vector3.right; 
        }

        if(isBeamOn){
            Vector3 movedir = dir * Time.deltaTime * move_speed;
            movedir.y = 0;
            movedir.z = 0;
            //transform.position += movedir;

            if(bounds.x < transform.position.x){
                dir = Vector3.left;
            }
            
            if(-bounds.x > transform.position.x){
                dir = Vector3.right;
            }

            if(timer >= BeamDuration){
                animator.Play("Idle");
                isBeamOn = false;
                timer = 0;
            }
        }

    }

    public void DamagePlayers(){
        IsHit = !IsHit;
    }
}

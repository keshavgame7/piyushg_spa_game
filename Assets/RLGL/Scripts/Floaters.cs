using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floaters : MonoBehaviour
{

    public Transform Target; // Main target the floater will automatically follow this. NO need to initialize.
    public float speed = 2f; //  to configure the how stricly the floater object follows the target.

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LateUpdate()
    {
        if(Target != null){
            Vector3 temp = Target.position;
            transform.position = Vector3.MoveTowards(transform.position,temp, Time.deltaTime * speed);
        }
        
    }
}

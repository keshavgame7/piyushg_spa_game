using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Visyde;
// #if HE_SYSCORE && STEAMWORKS_NET && !DISABLESTEAMWORKS
// using StatsClient = HeathenEngineering.SteamworksIntegration.API.StatsAndAchievements.Client;
// #else

// #endif

public enum AchievementType{
    Kills = 0,
    GrenedeKills = 1,
    MeleeKills = 2,
    Jumps = 3,
    Deaths = 4,
    Damage = 5,
    CoinsCollected = 6, 
    WeaponPicked = 7, 
    GrenedePicked = 8, 
    AmmoPicked = 9 ,
    KillSpree = 10

}

[System.Serializable]
public class Achievements{
    public string InGameId;
    public string EasyAchievementId;
    public string SteamworksId;

    public string DisplayName;
    public string Discription;
    public AchievementType type;
    public int amount;
}
public class AchievementManager : MonoBehaviour
{

    public Achievements[] achievements;
    public static AchievementManager manager;
    public GameObject AchievementPref;
    public Transform itemParent;

    public bool IsSteamWork;
    public bool IsLoggedIn;
    public AchievementNames[] AchievementNamesEnums;
    
    
    private void Awake()
    {
        if(manager == null){
            manager = this;
            DontDestroyOnLoad(manager.gameObject);
        }else{
            Destroy(this.gameObject);
        }

        #if DISABLESTEAMWORKS
            IsSteamWork = false;
        #else
            IsSteamWork = true;
        #endif

        // #if HE_SYSCORE && STEAMWORKS_NET && !DISABLESTEAMWORKS
        //     API.StatsAndAchievements.Client.SetAchievement(achievementName);
        // #else
            
        // #endif
        
    }

    // Start is called before the first frame update
    void Start()
    {
        //Invoke("As",5f);
        if(IsSteamWork){

        }else{
            int nrOfAchievements = System.Enum.GetValues(typeof(AchievementNames)).Length;
            AchievementNamesEnums = new AchievementNames[nrOfAchievements];
            for (int i = 0; i < nrOfAchievements; i++)
            {
                AchievementNamesEnums[i] = ((AchievementNames)i);
            }
            GameServices.Instance.LogIn(LoginComplete);
        }
        
    }

    public void LoginComplete(bool success){
        IsLoggedIn = success;
    }

    public void As(){
        foreach (var item in achievements)
        {
            Debug.Log(PlayerProfile.instance.data.IsAchivementUnlocked(item.InGameId));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartCheckAchievements(){
        StartCoroutine("CheckAchievements");
    }

    public IEnumerator CheckAchievements(){
        foreach (var item in achievements)
        {
            if(!PlayerProfile.instance.data.IsAchivementUnlocked(item.InGameId)){
                if(PlayerProfile.instance.data.GetAchievementProgress(item.type.ToString()) >= item.amount){
                    ShowAchievement(item);
                    yield return new WaitForSeconds(0.5f);
                }
            }
        }
        PlayerProfile.instance.Save();
    }


    public void ShowAchievement(Achievements achievement){
        if(IsSteamWork){
            #if HE_SYSCORE && STEAMWORKS_NET && !DISABLESTEAMWORKS
                //API.StatsAndAchievements.Client.SetAchievement(achievement.SteamworksId);
                PlayerProfile.instance.data.UnlockAchivement(achievement.InGameId);
                Debug.Log("Achievement unlocked : "+ achievement.InGameId);
                GameObject o = Instantiate(AchievementPref,GameObject.FindGameObjectWithTag("achievementParent").transform);
                o.GetComponent<AchievementItem>().Name.text = achievement.DisplayName;
                o.GetComponent<AchievementItem>().description.text = achievement.Discription;
                o.GetComponent<AchievementItem>().show();
            #else
                Debug.Log("Steam Error");
            #endif
        }else{
            if(IsLoggedIn){
                GameServices.Instance.SubmitAchievement(GetAchievementName(achievement.EasyAchievementId),(bool success,GameServicesError message)=>{
                    if(success){
                        PlayerProfile.instance.data.UnlockAchivement(achievement.InGameId);
                        Debug.Log("Achievement unlocked : "+ achievement.InGameId);
                        GameObject o = Instantiate(AchievementPref,GameObject.FindGameObjectWithTag("achievementParent").transform);
                        o.GetComponent<AchievementItem>().Name.text = achievement.DisplayName;
                        o.GetComponent<AchievementItem>().description.text = achievement.Discription;
                        o.GetComponent<AchievementItem>().show();
                    }else{
                        Debug.Log("Error in updating achivement");
                    }
                });
            }else{
                Debug.Log("Not Logged in");
            }
        }
        
    }

    public AchievementNames GetAchievementName(string name){
        foreach (var item in AchievementNamesEnums)
        {
            if(item.ToString() == name){
                return item;
            }
        }
        return new AchievementNames();
    }
}

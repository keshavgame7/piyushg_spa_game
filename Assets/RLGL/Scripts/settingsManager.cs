using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class settingsManager : MonoBehaviour
{

    public Toggle sfxTog;
    public GameObject sfxOff, sfxOn;
    public Toggle musicTog;
    public GameObject MusicOff, MusicOn;

    // Start is called before the first frame update
    void Start()
    {
        musicTog.isOn = PlayerPrefs.GetInt("Music",1) == 1;
        sfxTog.isOn = PlayerPrefs.GetInt("Sound",1) == 1;
        setMusic();
        setSound();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setMusic(){
        if(musicTog.isOn){
            PlayerPrefs.SetInt("Music",1);
            MusicManager.Get().SetVol(0.4f);
            MusicManager.Get().triggerMusic();
            if(SceneManager.GetActiveScene().name != "MainMenu"){
                MusicOn.SetActive(true);
                MusicOff.SetActive(false);
            }
        }else{
            PlayerPrefs.SetInt("Music",0);
            MusicManager.Get().SetVol(0);
            if(SceneManager.GetActiveScene().name != "MainMenu"){
                MusicOn.SetActive(false);
                MusicOff.SetActive(true);
            }
        }
    }

    public void setSound(){
        if(sfxTog.isOn){
            PlayerPrefs.SetInt("Sound",1);
            if(SceneManager.GetActiveScene().name != "MainMenu"){
                sfxOn.SetActive(true);
                sfxOff.SetActive(false);
            }
        }else{
            PlayerPrefs.SetInt("Sound",0);
            if(SceneManager.GetActiveScene().name != "MainMenu"){
                sfxOn.SetActive(false);
                sfxOff.SetActive(true);
            }
        }
    }
}

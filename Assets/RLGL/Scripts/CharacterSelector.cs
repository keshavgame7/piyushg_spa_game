using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

namespace Visyde
{
    /// <summary>
    /// Character Selector
    /// - displays a list of characters for the character selection screen
    /// </summary>

    public class CharacterSelector : MonoBehaviour
    {
        public CharacterSelectorItem itemPrefab;
        public CharacterData[] characters;
        public Transform content;
        public Connector connector;
        public SampleMainMenu mainMenu;
        bool isTablet = false;

        void Start()
        {
            PlayerProfile.instance.onProfileLoaded += Refresh;
            DataCarrier.characters = characters;
            SelectCharacter(characters[PlayerPrefs.GetInt("SelectedCharacter",0)]);
            if (Camera.main.aspect >= 1.7)
            {
                isTablet = false;
            //            Debug.Log("16:9");
            }
            else if (Camera.main.aspect >= 1.5)
            {
                isTablet = false;
            //          Debug.Log("3:2");
            }
            else
            {
                isTablet = true;
            //        Debug.Log("4:3");
            }

            if(isTablet){
                content.GetComponent<GridLayoutGroup>().cellSize = new Vector2(180f,388.16f);
            }else{
                content.GetComponent<GridLayoutGroup>().cellSize = new Vector2(294f,634f);
            }


        }

        void OnDisable()
        {
            PlayerProfile.instance.onProfileLoaded -= Refresh;
        }

        /// <summary>
        /// Refresh the character selection window.
        /// </summary>
        public void Refresh()
        {
            // Clear items:
            foreach (Transform t in content)
            {
                Destroy(t.gameObject);
            }

            // Repopulate items:
            for (int i = 0; i < characters.Length; i++)
            {
                CharacterSelectorItem item = Instantiate(itemPrefab, content);
                item.data = characters[i];
                item.cs = this;
            }
        }

        // Character selection:
        public void SelectCharacter(CharacterData data)
        {
            // Close the character selection panel:
            mainMenu.characterSelectionPanel.SetActive(false);

            // ...then set the "character using" in the DataCarrier:
            for (int i = 0; i < characters.Length; i++)
            {
                if (data == characters[i])
                {
                    DataCarrier.chosenCharacter = i;
                    //Add the character to photon properties
                    ExitGames.Client.Photon.Hashtable p = PhotonNetwork.LocalPlayer.CustomProperties;
                    if(p.ContainsKey("character"))
                        p["character"] = DataCarrier.chosenCharacter;
                    else
                        p.Add("character",DataCarrier.chosenCharacter);
                    PlayerPrefs.SetInt("SelectedCharacter",DataCarrier.chosenCharacter);
                    PhotonNetwork.LocalPlayer.CustomProperties = p;
                }
            }

            
            mainMenu.characterIconPresenter.sprite = data.icon;
        }
    }
}

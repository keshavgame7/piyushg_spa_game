using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace Visyde
{
    /// <summary>
    /// Grenade Controller
    /// - The primary component of a grenade game object. Unlike the projectile, grenades are spawned
    /// across the network and not locally so that any movement can be synced between clients.
    /// </summary>

    public class GrenadeController : MonoBehaviourPunCallbacks, IPunObservable
    {
        [Header("Settings:")]
        public int damage;
        public float radius;
        public float delay;
        public AudioClip impactSound;

        [Space]
        [Header("References:")]
        public AudioSource aus;
        public GameObject explosionEffect;
        public GameObject graphic;
        public Rigidbody2D rg;
        
        [HideInInspector] public PlayerInstance owner;

        // Network:
        Vector2 moveTo;
        float rotTo;
        //Tutorial related instantiation data
        public Vector2 direction;
        public int playerId;

        public int Id;

        public static List<int> grenade_Ids = new List<int>();

        private void Awake()
        {
            int i = 0;
            if(grenade_Ids == null)
                grenade_Ids = new List<int>();
            while(grenade_Ids.Contains(i)){
                i = Random.Range(0,1000);
            }
            Id = i;
            grenade_Ids.Add(Id);
        }

        // Use this for initialization
        void Start()
        {

            if(!GameManager.instance.IsTutorial)
            {// Only the owner explodes:
                if(photonView.IsMine)
                {
                    // Force:
                    Vector2 throwDir = (Vector2)photonView.InstantiationData[0];
                    rg.AddForce(throwDir,ForceMode2D.Impulse);

                    Invoke("ExplodeCallFromOwner",delay);
                }
                owner = GameManager.instance.GetPlayerInstance((int)photonView.InstantiationData[1]);
            }
            else 
            {
                SetGrenadeData(direction,playerId);
            }
        }

        // Update is called once per frame
        void Update()
        {
            // Positioning, rotation etc.:
            if (photonView.IsMine || GameManager.instance.IsTutorial)
            {
                //moveTo = rg.position;
                //rotTo = rg.rotation;
            }
            else
            {
                //transform.position = Vector3.MoveTowards (transform.position, moveTo, Time.deltaTime * 10);
                //rg.rotation = Mathf.MoveTowards (rg.rotation, rotTo, Time.deltaTime * 400);
                rg.gravityScale = 0;
            }
        }

        void OnCollisionEnter2D(Collision2D col)
        {
            if (photonView.IsMine && ((GameManager.instance.ourPlayer && col.transform.root != GameManager.instance.ourPlayer.transform) || !GameManager.instance.ourPlayer))
            {
                photonView.RPC("CollisionSound", RpcTarget.All);
            }
        }

        [PunRPC]
        public void CollisionSound()
        {
            if(PlayerPrefs.GetInt("Sound",1) == 1)
                aus.PlayOneShot(impactSound);
        }

        void ExplodeCallFromOwner()
        {
            if(GameManager.instance.IsTutorial) 
            {
                Explode();
            }
            else 
            {
                photonView.RPC("Explode",RpcTarget.All);
            }
            
        }

        // Check if player is in hit radius based on position
        bool IsInside(Vector3 pos){
            return (((pos.x - transform.position.x) * (pos.x - transform.position.x)) + ((pos.y - transform.position.y) * (pos.y - transform.position.y)) <= (radius * radius));
        }



        [PunRPC]
        public void Explode()
        {
            // VFX:
            GameObject o = Instantiate(explosionEffect, transform.position, Quaternion.identity);
            if(o.GetComponent<AudioSource>() != null)
                o.GetComponent<AudioSource>().volume = PlayerPrefs.GetInt("Sound",1);


            // Disable collider and graphic:
            GetComponent<Collider2D>().enabled = false;
            graphic.SetActive(false);

            // Damaging:
            if (photonView.IsMine || GameManager.instance.IsTutorial)
            {
                // Loop all player and check if they are in damage radius
                foreach (var p in GameObject.FindObjectsOfType<PlayerController>())
                {
                    if (((p.photonView.Owner == photonView.Owner && GameManager.instance.allowHurtingSelf) || p.photonView.Owner != photonView.Owner) && !p.invulnerable)
                    {
                        if (!p.isDead && IsInside(p.transform.position))
                        {
                            // Calculate the damage based on distance:
                            int finalDamage = Mathf.RoundToInt(damage * (1 - ((transform.position - new Vector3(p.transform.position.x, p.transform.position.y)).magnitude / radius)));
                            // Apply damage:
                            p.ApplyDamage(owner.playerID, finalDamage, false,false,true);
                        }
                    }
                }

                // Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, radius);
                // for (int i = 0; i < cols.Length; i++)
                // {
                //     if (cols[i].CompareTag("Player"))
                //     {
                //         PlayerController p = cols[i].GetComponent<PlayerController>();
                //         if (((p.photonView.Owner == photonView.Owner && GameManager.instance.allowHurtingSelf) || p.photonView.Owner != photonView.Owner) && !p.invulnerable)
                //         {
                //             if (!p.isDead)
                //             {
                //                 Vector2 grPos = new Vector2(transform.position.x, transform.position.y);
                //                 RaycastHit2D[] hits = Physics2D.RaycastAll(grPos, new Vector2(cols[i].transform.position.x, cols[i].transform.position.y) - grPos, radius);
                //                 RaycastHit2D hit = new RaycastHit2D();
                //                 for (int h = 0; h < hits.Length; h++)
                //                 {
                //                     if (hits[h].collider.gameObject == cols[i].gameObject)
                //                     {
                //                         hit = hits[h];
                //                         // Calculate the damage based on distance:
                //                         int finalDamage = Mathf.RoundToInt(damage * (1 - ((transform.position - new Vector3(hit.point.x, hit.point.y)).magnitude / radius)));
                //                         // Apply damage:
                //                         p.ApplyDamage(owner.playerID, finalDamage, false);
                //                         break;
                //                     }
                //                 }
                //             }
                //         }
                //     }
                // }

                // Destroy:
                PhotonNetwork.Destroy(photonView);
            }
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting && (photonView.IsMine || GameManager.instance.IsTutorial))
            {
                // Send position over network
                //stream.SendNext (moveTo);
                //stream.SendNext (rotTo);
            }
            else if (stream.IsReading)
            {
                // Receive positions
                //moveTo = (Vector2)stream.ReceiveNext();
                //rotTo = (float)stream.ReceiveNext ();
            }
        }

        public void Teleport(Vector3 newPos)
        {
            transform.position = newPos;
        }

        void OnDrawGizmos()
        {
            Gizmos.color = new Color(1, 0, 0, 0.3f);
            Gizmos.DrawSphere(transform.position, radius);
        }
        public void SetGrenadeData( Vector2 dir,int playerId) 
        {
            Vector2 throwDir = dir;
            rg.AddForce(throwDir,ForceMode2D.Impulse);

            Invoke("ExplodeCallFromOwner",delay);
            owner = GameManager.instance.GetPlayerInstance(playerId);
        }
    }
}

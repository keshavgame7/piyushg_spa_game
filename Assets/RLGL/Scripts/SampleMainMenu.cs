using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;

namespace Visyde
{
    /// <summary>
    /// Sample Main Menu
    /// - A sample script that handles the main menu UI.
    /// </summary>

    public class SampleMainMenu : MonoBehaviour
    {
        public static SampleMainMenu instance;

        [Header("Main:")]
        public Text connectionStatusText;
        public Button findMatchBTN;
        public Button customMatchBTN;
        public GameObject findMatchCancelButtonObj;
        public GameObject findingMatchPanel;
        public GameObject customGameRoomPanel;
        public Text matchmakingPlayerCountText;
        public InputField playerNameInput;
        public GameObject messagePopupObj;
        public GameObject messagePopupObjMobile;
        public GameObject IAPpanel;
        public Text messagePopupText;
        public GameObject characterSelectionPanel;
        public Image characterIconPresenter;
        public GameObject loadingPanel;
        public Toggle frameRateSetting;
        public AudioSource aus;

        [Header("Purchase Screen:")]
        public string notEnoughCoinsMessage;
        public string purchaseScreenMessage;
        public AudioClip errorSFX;
        public AudioClip successSFX;
        public GameObject purchasePanel;
        public Image itemPreview;
        public Text purchaseMessageText;

        [Header("Player Account:")]
        public Text goldCoinsText;
        public GameObject profileContents;
        public GameObject profileLoadingImage;

        public ShopItemEntity curPurchasing;

        public Scrollbar bar;
        public ScrollRect view;

        public GameObject CharImg;

        
        
        public void UpdateBar(){

            bar.value = view.horizontalNormalizedPosition;
        }

         

        void Awake()
        {
            Screen.sleepTimeout = SleepTimeout.SystemSetting;

            instance = this;
        }

        void OnEnable()
        {

        }
        void OnDisable()
        {
            PlayerProfile.instance.onProfileLoaded -= ProfileLoaded;
        }

        // Use this for initialization
        void Start()
        {
            PlayerProfile.instance.onProfileLoaded += ProfileLoaded;

            // Load or create a username:
            if (PlayerPrefs.HasKey("name"))
            {
                playerNameInput.text = PlayerPrefs.GetString("name");
            }
            else
            {
                playerNameInput.text = "Player" + Random.Range(0, 9999);
            }
            SetPlayerName();

            // Others:
            frameRateSetting.isOn = Application.targetFrameRate == 60;

            Cursor.visible = true;
        }

        // Update is called once per frame
        void Update()
        {
            bool connecting = !PhotonNetwork.IsConnectedAndReady || PhotonNetwork.NetworkClientState == ClientState.ConnectedToNameServer || PhotonNetwork.InRoom;

            // Handling texts:
            connectionStatusText.text = connecting ? PhotonNetwork.NetworkClientState == ClientState.ConnectingToGameServer ? "Connecting..." : "Finding network..."
                : "Connected! (" + PhotonNetwork.CloudRegion + ") | Ping: " + PhotonNetwork.GetPing();
            connectionStatusText.color = PhotonNetwork.IsConnectedAndReady ? Color.green : Color.yellow;
            matchmakingPlayerCountText.text = PhotonNetwork.InRoom ? Connector.instance.totalPlayerCount + "/" + PhotonNetwork.CurrentRoom.MaxPlayers : "Matchmaking...";

            // Handling buttons:
            customMatchBTN.interactable = !connecting;
            findMatchBTN.interactable = !connecting;
            if(findMatchCancelButtonObj != null)
                findMatchCancelButtonObj.SetActive(PhotonNetwork.InRoom);

            // Handling panels:
            
            customGameRoomPanel.SetActive(Connector.instance.isInCustomGame);
            loadingPanel.SetActive(PhotonNetwork.NetworkClientState == ClientState.ConnectingToGameServer || PhotonNetwork.NetworkClientState == ClientState.DisconnectingFromGameServer);

            // Messages popup system (used for checking if we we're kicked or we quit the match ourself from the last game etc):
            if (DataCarrier.message.Length > 0)
            {
                messagePopupObj.SetActive(true);
                messagePopupText.text = DataCarrier.message;
                DataCarrier.message = "";
            }
        }

        // Profile:
        public void SetPlayerName()
        {
            PlayerPrefs.SetString("name", playerNameInput.text);
            PhotonNetwork.NickName = playerNameInput.text;
        }
        void ProfileLoaded()
        {
            profileContents.SetActive(true);
            profileLoadingImage.SetActive(false);

            goldCoinsText.text = PlayerProfile.instance.data.goldCoins.ToString();

            
        }

        // Main:

        public void FindMatch()
        {
            // Enable the "finding match" panel:
            findingMatchPanel.SetActive(true);
            // ...then finally, find a match:
            Connector.instance.FindMatch();
        }
        public void OpenPurchaseScreen(ShopItemEntity itemToPurchase)
        {

            if (!PlayerProfile.instance.IsItemAffordable(itemToPurchase))
            {
                //Check on which platform the game is running. 
                if(Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android){
                    OpenMobileMsg();
                }else{
                    DataCarrier.message = notEnoughCoinsMessage;
                    PlaySFX(errorSFX);
                }
                
                return;
            }

            CharImg.SetActive(itemToPurchase.itemType == ItemType.Character);

            curPurchasing = itemToPurchase;

            purchasePanel.SetActive(true);
            /* purchaseMessageText.text = "Purchase this " + (itemToPurchase.itemType == ItemType.Weapon? "weapon" : itemToPurchase.itemType == ItemType.Map? "map" : "character")
            + " for <color=yellow>" + itemToPurchase.cost + "</color> gold coins?"; */
            purchaseMessageText.text = purchaseScreenMessage;
            itemPreview.sprite = itemToPurchase.itemType == ItemType.Weapon ? itemToPurchase.weapon.hudIcon : itemToPurchase.itemPreview;
        }

        public void OpenMobileMsg(){
            
            messagePopupObjMobile.SetActive(true);
        }

        public void OpenIAP(){
            IAPpanel.SetActive(true);
        }

        public void PlaySFX(AudioClip clip){
            if(PlayerPrefs.GetInt("Sound",1) == 1)
                aus.PlayOneShot(clip);
        }

        public void DoPurchase()
        {
            if (PlayerProfile.instance.TryAcquireItem(curPurchasing.id))
            {
                DataCarrier.message = "Purchase success!";
                PlaySFX(successSFX);
                
                ClosePurchaseScreen();
            }
            else
            {
                DataCarrier.message = "Transaction error!";
                PlaySFX(errorSFX);
                
            }
        }
        public void ClosePurchaseScreen()
        {
            purchasePanel.SetActive(false);
            curPurchasing = null;
        }

        // Others:
        // *called by the toggle itself in the "On Value Changed" event:
        public void ToggleTargetFps()
        {
            Application.targetFrameRate = frameRateSetting.isOn ? 60 : 30;

            // Display a notif message:
            if (frameRateSetting.isOn)
            {
                DataCarrier.message = "Target frame rate has been set to 60.";
            }
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialManager : MonoBehaviour
{

    private static TutorialManager manager;

    public List<BotSettings> tutorialBots;
    public static TutorialManager Get(){
        return manager;
    }
    private void Awake()
    {
        if(manager == null){
            manager = this;
            DontDestroyOnLoad(this.gameObject);
        }else{
            Destroy(this.gameObject);
        }
    }

    [Tooltip("This is the index of the map form the connector script list that you want in the tutorial.")]
    public int TutorialMapId;

    public void StartTutorial(){
        SceneManager.LoadScene("Tutorial");
    }
}

[System.Serializable]
public class BotSettings{
    [SerializeField]
    [Range(0, 10)]
    public int Difficulty;
    [SerializeField]
    public int character;
    [SerializeField]
    public int cosmetic;

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Visyde;

public class MapItem : MonoBehaviour
{

    public Image MapImg;
    public Text MapText;
    public Text RecommendText;
    public Text CostText;

    public string MapName;

    public int MapId;

    public GameObject LockedOverlay; // to show locked maps

    public bool IsSelected;

    public GameObject HighlightEff;
    
    
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    

    public void Set(int id,Sprite image, string name,string cost, int minPL, int maxPL){
        MapId = id;
        MapImg.sprite = image;
        MapText.text = name;
        CostText.text = cost;
        RecommendText.text = "Recommended players "+minPL+"-"+maxPL;
        MapName = name;
    }

    public void OnClickMap(){
        if(LockedOverlay.activeInHierarchy)
            return;

        foreach (Transform item in transform.parent)
        {
            LeanTween.scale(item.gameObject,Vector3.one,0.2f);
            item.GetComponent<MapItem>().IsSelected = false;
            item.GetComponent<MapItem>().OnSelect();
        }
        
        if(IsSelected){
            LeanTween.scale(gameObject,Vector3.one,0.2f);
            IsSelected = false;
        }else{
            LeanTween.scale(gameObject,Vector3.one * 1.1f,0.2f);
            LobbyBrowserUI.instance.SelectMap(MapId);
            IsSelected = true;
        }
        OnSelect();
    }

    public void OnSelect(){
        HighlightEff.SetActive(IsSelected);
    }

    public void BuyMap(){
        SampleMainMenu.instance.OpenPurchaseScreen(GlobalShopItems.instance.GetMap(MapName));
    }

}
